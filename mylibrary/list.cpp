#include "list.h"



namespace mylib{

    //constructor
    List::List(std::initializer_list<int> List): elements(0), head(nullptr), tail(nullptr)
    {
        for (std::initializer_list<int>::iterator it=List.begin(); it!=List.end() ; ++it)
        {
            if(!head){
                //std::unique_ptr<node> newNode(node(*it, nullptr, nullptr));
                 //head = std::move(newNode);
                head = std::make_shared<node>(node(*it, nullptr, nullptr));

                elements++;
            }
            else{
            std::shared_ptr<node> last = head;
                while(last->next){
                    last = last->next;
                }
               // std::unique_ptr<node> newNode(node(*it, nullptr, nullptr));
                std::shared_ptr<node> newNode = std::make_shared<node>(node(*it, nullptr, nullptr));
                last->next = std::move(newNode);
                newNode->prev = last;
                newNode->next = nullptr;
                elements++;
            }
        }
    }

    //Destructor
    List::~List() {}

    //Begin
   std::shared_ptr< List::node> List::begin() {
        return head;
    }
    //End
    std::shared_ptr<List::node> List::end()  {
        return tail;
    }

    //Front
    int& List:: front() {

        return head->data;
    }
    //Back

    int& List:: back() {

        return tail->data;
    }

    //Push back
    void List::push_back(int data) {
        //std::unique_ptr<node> newNode(node(data, nullptr, tail));
        shared_ptr<node> newNode = std::make_shared<node>(node(data, nullptr, tail));
        if(!head)
            head = std::move(newNode);
        if(tail)
        tail->next = std::move(newNode);
        tail = std::move(newNode);
        ++elements;
    }

    //Erase a node in double linked list
    void List::erase(){

    }
    //Push front
    void List::push_front(int data) {
        //std::unique_ptr<node> newNode(node(data, head, nullptr));
        shared_ptr<node> newNode = std::make_shared<node>(node(data, head, nullptr));
        if(!tail)
            tail = std::move(newNode);
        if(head)
            head->prev = std::move(newNode);
        head = std::move(newNode);
        ++elements;
    }


    bool List::empty() const {
        return head == nullptr;
    }

    size_t List::size() const {
        return elements;
    }


}
