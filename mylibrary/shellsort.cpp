#include <gtest/gtest.h>

#include "shellsort.h"

#include <vector>
#include <algorithm>


/*
// Helper function
template <typename T>
::testing::AssertionResult VectorsMatch( const std::vector<T>& expected, const std::vector<T>& actual ){

  // Test size
  if( expected.size() != actual.size() )
    return ::testing::AssertionFailure() << "size mismatch: |vector| (" << actual.size() << ") != |expected| (" << expected.size() << ")";

  // Test per element content
  for( typename std::vector<T>::size_type i {0}; i < expected.size(); ++i )
    if( expected.at(i) != actual.at(i) )
      return ::testing::AssertionFailure() << "vector[" << i
                                           << "] (" << actual.at(i) << ") != expected[" << i
                                           << "] (" << expected.at(i) << ")";

  return ::testing::AssertionSuccess();
}


// Vector conatiner initialization tests
TEST(Algorithm_ShellSort,SortTest_01) {

  // Define gold
  std::initializer_list<int> gold_lst {4,1,3,2};
  std::initializer_list<int> gold_lst_sorted {1,2,3,4};
  std::initializer_list<int> gold_lst_sorted_reverse {4,3,2,1};
  std::vector<int>           gold_sorted {gold_lst_sorted};
  std::vector<int>           gold_sorted_reverse {gold_lst_sorted_reverse};


  // Construct
  std::vector<int> vec {gold_lst};

  // TEST: Sort the vector using < comparison (default)
  mylib::shellSort(vec.begin(),vec.end());
  EXPECT_TRUE (VectorsMatch(gold_sorted,vec));

  // TEST: Sort the vector using > comparison
  mylib::shellSort(vec.begin(),vec.end(),std::greater<int>());
  EXPECT_TRUE (VectorsMatch(gold_sorted_reverse,vec));

}
*/
