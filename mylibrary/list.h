#ifndef LIST_H
#define LIST_H
#include<iostream>
#include<memory>

#include <initializer_list>
using namespace std;

namespace mylib{

    class List {
        public:
        List(){}
        List(std::initializer_list<int> List);
        List  & operator = (const List &);
        ~List();

        void push_back(int data);
        void push_front(int data);
        void erase();

        size_t size() const;
        bool empty() const;

        int& front();

        int& back();

        struct node {
            int data;
            shared_ptr<node> next, prev;
            node(int const& data, std::shared_ptr<node> next, std::shared_ptr<node> prev)
            : data(data)
            , next(next)
            , prev(prev) {
            }

        };
        int elements;
        std::shared_ptr<node> head;
        std::shared_ptr<node> tail;

        typedef shared_ptr<node> iterator;
        typedef shared_ptr<node> const const_iterator;

        iterator begin();

        iterator end();

    };
}


#endif // LIST_H
