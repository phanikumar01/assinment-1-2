#ifndef SORT_H_INCLUDED
#define SORT_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <typeinfo>

using namespace std;

class cSort_t
{
public:
    template <typename T>
    static void insertionSort (T* _array, const unsigned size);

    template <typename T>
    static void introSort (T* _array, const unsigned size);

    template <typename T>
    static void heapSort (T* _array, const unsigned size);

    template <typename T>
    static void buildHeap (T* _array, const unsigned size);

protected:
    cSort_t ();
    ~cSort_t ();
    cSort_t (const cSort_t& ref);
    const cSort_t& operator= (const cSort_t& ref);

private:
    template <typename T>
    static void partition (T* _array, const unsigned size, unsigned& pivotIndex);

    template <typename T>
    static void swap (T* _target, T* _source, const unsigned size);

    static unsigned choosePivot (const unsigned size);

    template <typename T>
    static void maxHeapify (T* _array, unsigned index, unsigned size);

    static unsigned recursionDepth;
};


//----------------- Insertion Sort --------------------
template <typename T>
void cSort_t::insertionSort (T* _array, const unsigned size)
{
    T* _temp = 0;
    unsigned TSize = sizeof(T);

    try
    {
        _temp = (T*) (::operator new (TSize));
    }
    catch (bad_alloc& ba)
    {
        cerr << "Error in Insertion sort function: Can't allocate memory: " << ba.what() << endl;
        throw;
    }

    for (int unsorted = 1; unsorted<(int)size; ++unsorted)
    {
        memcpy (_temp, _array+unsorted, TSize);

        for (int index = unsorted-1; index>=0; --index)
        {
            if (*_temp < *(_array+index))
            {
                memcpy (_array+index+1, _array+index, TSize);
                if (0==index)
                    memcpy (_array, _temp, TSize);
            }
             else
             {
                memcpy (_array+index+1, _temp, TSize);
                break;
             }
        }
    }
    ::operator delete(_temp);
    _temp = 0;
}


//----------------- INTROSORT--------------------
template <typename T>
void cSort_t::introSort (T* _array, const unsigned size)
{
    static const unsigned minQuickSize = 4;
    static const unsigned maxRecursionDepth = (unsigned) (log(size) / log(2)) + 1;

    if (minQuickSize < size)
    {
        unsigned pivotIndex = choosePivot(size);

        partition<T> (_array, size, pivotIndex);

        if (minQuickSize < pivotIndex)
        {
            if (recursionDepth <= maxRecursionDepth)
            {
                ++recursionDepth;
                introSort<T> (_array, pivotIndex);
                --recursionDepth;
            }
            else
            {
                heapSort<T> (_array, pivotIndex);
            }
        }
        else if (1 < pivotIndex)
        {
            insertionSort<T> (_array, pivotIndex);
        }

        if (minQuickSize < (size-pivotIndex-1))
        {
            if (recursionDepth <= maxRecursionDepth)
            {
                ++recursionDepth;
                introSort<T> (_array+pivotIndex+1, size-pivotIndex-1);
                --recursionDepth;
            }
            else
                heapSort<T> (_array+pivotIndex+1, size-pivotIndex-1);
        }
        else if (1 < (size-pivotIndex-1))
        {
            insertionSort<T> (_array+pivotIndex+1, size-pivotIndex-1);
        }
    }
    else
    {
       insertionSort<T> (_array, size);
    }
}


//----------------- Partition Function --------------------
template <typename T>
void cSort_t::partition (T* _array, const unsigned size, unsigned& pivotIndex)
{
    unsigned TSize = sizeof(T);

    swap<T> (_array, _array+pivotIndex, TSize);

    pivotIndex = 0;

    for (unsigned unknownIn=1; unknownIn<size; ++unknownIn)
    {
        if (_array[unknownIn] < _array[0])
        {
            swap<T> (_array+pivotIndex+1, _array+unknownIn, TSize);
            ++pivotIndex;
        }
    }

    swap<T> (_array, _array+pivotIndex, TSize);
}


//----------------- Swap Function --------------------
template <typename T>
void cSort_t::swap (T* _target, T* _source, const unsigned size)
{
    if (sizeof(T) < 128)
    {
       T temp;

       temp = *_target;
       *_target = *_source;
       *_source = temp;
    }
    else
    {
        T* _temp = 0;

        try
        {
            _temp = (T*)::operator new (size);
        }
        catch (bad_alloc& baRef)
        {
            cerr << "Error in Swap function: Can't allocate memory: " << baRef.what() << endl;
            throw;
        }

        memcpy (_temp, _target, size);
        memcpy (_target, _source, size);
        memcpy (_source, _temp, size);

        ::operator delete (_temp);
        _temp = 0;
    }
}


//----------------- Heap Sort --------------------
template <typename T>
void cSort_t::heapSort (T* _array, const unsigned size)
{
    if (size < 2)
    {
        cout << "Array has wrong size" << endl;
        return;
    }

    buildHeap (_array, size);

    unsigned TSize = sizeof(T);

    swap (_array, _array+size-1, TSize);

    for (unsigned unsortedSize=size-1; 1<unsortedSize; --unsortedSize)
    {
        maxHeapify (_array, 0, unsortedSize);
        swap (_array, _array+unsortedSize-1, TSize);
    }
}


//----------------- Build Heap --------------------
template <typename T>
void cSort_t::buildHeap (T* _array, const unsigned size)
{
    if (size < 2)
    {
        cout << "Array has wrong size" << endl;
        return;
    }

    for (int parent=(size-2)/2; 0<=parent; --parent)
    {
        maxHeapify (_array, (unsigned)parent, size);
    }
}


//----------------- Max Heapify Function --------------------
template <typename T>
void cSort_t::maxHeapify (T* _array, unsigned index, unsigned size)
{
    unsigned parent = (size-2)/2;

    if (parent < index)
        return;

             parent = index;
    unsigned child = 2*index + 1;
    unsigned rightChild = child + 1;
    unsigned TSize = sizeof(T);

    if ((rightChild < size)  &&  (_array[child] < _array[rightChild]))
        child = rightChild;

    if (_array[parent] < _array[child])
    {
        swap (_array+parent, _array+child, TSize);
        maxHeapify (_array, child, size);
    }
}

#endif // SORT_H_INCLUDED
